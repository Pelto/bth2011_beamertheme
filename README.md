BTH Beamer Theme
=====

This is not an official template from BTH, please use the original source
instead at [https://bitbucket.org/mickesv/bth2011_beamertheme](https://bitbucket.org/mickesv/bth2011_beamertheme). 
